﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Common.Actions;
using System;
using System.Diagnostics;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class PowerActionTests
    {
        [TestCleanup]
        public void TestCleanup()
        {
            RunShutdownProcess("/a");
        }

        private string SetTimeArgument(string originalArg)
        {
            return originalArg.Replace("0", "3600");
        }

        private string AppendTimeArgument(string originalArg)
        {
            return $"{originalArg} /t 3600";
        }

        private int RunShutdownProcess(string argument)
        {
            Console.WriteLine($"Given argument: \"{argument}\"");
            var p = new Process();
            p.StartInfo.FileName = "shutdown.exe";
            p.StartInfo.Arguments = argument;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.UseShellExecute = true;
            p.Start();
            p.WaitForExit();
            return p.ExitCode;
        }

        //NOTE: commented out because "/t 3600" is not valid for this action.
        //Without specifying the timeout, the computer WILL hibernate.
        //[TestCategory("PowerActionTests"), TestMethod]
        //public void TestHibernate()
        //{
        //    var exitCode = RunShutdownProcess(PowerAction.HIBERNATE.Argument);
        //    Assert.AreEqual(0, exitCode);
        //}

        [TestCategory("PowerActionTests"), TestMethod]
        public void TestHybrid()
        {
            var argument = SetTimeArgument(PowerAction.HYBRID.Argument);
            var exitCode = RunShutdownProcess(argument);
            Assert.AreEqual(0, exitCode);
        }

        //NOTE: commented out because "/t 3600" is not valid for this action.
        //Without specifying the timeout, the user WILL get logged off.
        //[TestCategory("PowerActionTests"), TestMethod]
        //public void TestLogoff()
        //{
        //    var exitCode = RunShutdownProcess(PowerAction.LOGOFF.Argument);
        //    Assert.AreEqual(0, exitCode);
        //}

        [TestCategory("PowerActionTests"), TestMethod]
        public void TestRestart()
        {
            var argument = SetTimeArgument(PowerAction.RESTART.Argument);
            var exitCode = RunShutdownProcess(argument);
            Assert.AreEqual(0, exitCode);
        }

        [TestCategory("PowerActionTests"), TestMethod]
        public void TestRestartAdvanced()
        {
            var argument = SetTimeArgument(PowerAction.RESTART_ADVANCED.Argument);
            var exitCode = RunShutdownProcess(argument);
            Assert.AreEqual(0, exitCode);
        }

        [TestCategory("PowerActionTests"), TestMethod]
        public void TestRestartReopen()
        {
            var argument = SetTimeArgument(PowerAction.RESTART_REOPEN.Argument);
            var exitCode = RunShutdownProcess(argument);
            Assert.AreEqual(0, exitCode);
        }
        
        [TestCategory("PowerActionTests"), TestMethod]
        public void TestShutdown()
        {
            var argument = SetTimeArgument(PowerAction.SHUTDOWN.Argument);
            var exitCode = RunShutdownProcess(argument);
            Assert.AreEqual(0, exitCode);
        }

    }
}
