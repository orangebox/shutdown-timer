﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Common.Helpers;
using System;
using System.Collections.Generic;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class ExceptionExtensionTests
    {
        [TestCategory("ExceptionExtensionTests"), TestMethod]
        public void TestFlatten()
        {
            var ex3 = new Exception("3rd exception");
            var ex2 = new Exception("2nd exception", ex3);
            var ex1 = new Exception("1st exception", ex2);

            var actual = ex1.Flatten();
            var expected = new List<Exception>()
            {
                ex1,
                ex2,
                ex3
            };

            //NOTE: The assertion is written this way because I
            //can't simply write Assert.AreEqual(expected, actual) -
            //That always fails for some reason.
            if (actual.Count != expected.Count)
                Assert.Fail("Sizes are different");
            for (int index = 0; index < actual.Count; index++)
            {
                if (actual[index] != expected[index])
                    Assert.Fail($"Objects at index {index} are not equal");
            }
        }
    }
}
