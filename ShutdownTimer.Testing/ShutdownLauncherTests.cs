﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Server.ProcessLib;
using ShutdownTimer.Common.Actions;
using System.Threading.Tasks;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class ShutdownLauncherTests
    {
        private readonly TimeSpan QUARTER_SECOND = new TimeSpan(0, 0, 0, 0, 250);
        private readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
        private readonly TimeSpan ONE_MINUTE = new TimeSpan(0, 1, 0);
        private readonly ShutdownLauncher SL = ShutdownLauncher.Instance;

        private ScheduledPowerAction BuildSpa(TimeSpan ts)
        {
            return BuildSpa(DateTime.Now.Add(ts));
        }
        private ScheduledPowerAction BuildSpa(DateTime dt)
        {
            return new ScheduledPowerAction(PowerAction.SHUTDOWN, dt);
        }

        /// <summary>
        /// Calls the <see cref="ShutdownLauncher.Abort()"/> method and ensures the <see cref="ShutdownLauncher.Action"/>
        /// property is null
        /// </summary>
        /// <returns></returns>
        [TestCategory("ShutdownLauncherTests"), TestMethod]
        public async Task TestAbort()
        {
            SL.Run(BuildSpa(ONE_MINUTE));
            await Task.Delay(QUARTER_SECOND);
            SL.Abort();
            Assert.IsNull(SL.Action);
        }

        /// <summary>
        /// Simulates closing the app and resuming it to ensure that the <see cref="ShutdownLauncher.Action"/>
        /// property is null.
        /// </summary>
        /// <returns></returns>
        [TestCategory("ShutdownLauncherTests"), TestMethod]
        public async Task TestContinue()
        {
            SL.Run(BuildSpa(ONE_MINUTE));
            await Task.Delay(QUARTER_SECOND);  //simulates closing the app
            Assert.IsNotNull(SL.Action);
        }

        /// <summary>
        /// Tests the countdown loop in the <see cref="ShutdownLauncher.Run(ScheduledPowerAction)"/> method
        /// Keep in mind that this test will take a minute to complete.
        /// </summary>
        /// <returns></returns>
        [TestCategory("ShutdownLauncherTests"), TestMethod]
        public async Task TestCountdown()
        {
            SL.Run(BuildSpa(new TimeSpan(0, 2, 0)));
            await Task.Delay(ONE_MINUTE);
            Assert.AreEqual(ONE_MINUTE, SL.Action.TimeToAction);
        }
    }
}
