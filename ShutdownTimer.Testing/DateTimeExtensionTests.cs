﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Common.Helpers;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class DateTimeExtensionTests
    {
        private readonly string DT_FORMAT = "yyyy-MM-dd HH:mm:ss.ffff";
        private readonly DateTime DATE_TIME = new DateTime(1987, 7, 16, 2, 10, 12, 12, DateTimeKind.Local);

        private DateTime TrimTime(long ticksToRound)
        {
            var dt = DATE_TIME.Trim(ticksToRound);
            Console.WriteLine($"{nameof(ticksToRound)}: {ticksToRound.ToString("N0")}");
            Console.WriteLine($"Original DateTime: {DATE_TIME.ToString(DT_FORMAT)}");
            Console.WriteLine($"New DateTime: {dt.ToString(DT_FORMAT)}");
            return dt;
        }

        /// <summary>
        /// Sets the millisecond component of a <see cref="DateTime"/> to 0
        /// </summary>
        [TestCategory("DateTimeExtensionTests"), TestMethod]
        public void TestTrimMilliSeconds()
        {
            var dt = TrimTime(TimeSpan.TicksPerSecond);
            Assert.AreEqual(0, dt.Millisecond);
        }

        /// <summary>
        /// Sets the second component of a <see cref="DateTime"/> to 0
        /// </summary>
        [TestCategory("DateTimeExtensionTests"), TestMethod]
        public void TestTrimSeconds()
        {
            var dt = TrimTime(TimeSpan.TicksPerMinute);
            Assert.AreEqual(0, dt.Second);
        }

        /// <summary>
        /// Sets the minute component of a <see cref="DateTime"/> to 0
        /// </summary>
        [TestCategory("DateTimeExtensionTests"), TestMethod]
        public void TestTrimMinutes()
        {
            var dt = TrimTime(TimeSpan.TicksPerHour);
            Assert.AreEqual(0, dt.Minute);
        }

        /// <summary>
        /// Sets the hour component of a <see cref="DateTime"/> to 0
        /// </summary>
        [TestCategory("DateTimeExtensionTests"), TestMethod]
        public void TestTrimHours()
        {
            var dt = TrimTime(TimeSpan.TicksPerDay);
            Assert.AreEqual(0, dt.Hour);
        }
    }
}
