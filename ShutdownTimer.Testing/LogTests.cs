﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Utils.Logging;
using System;
using System.IO;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class LogTests
    {
        #region Variables
        private static string logFilePath;
        #endregion Variables

        #region Before and After
        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            var desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            logFilePath = Path.Combine(desktop, "logfile.txt");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (File.Exists(logFilePath) == true)
                File.Delete(logFilePath);
        }
        #endregion Before and After

        #region LogEntry Tests
        /// <summary>
        /// Creates a <see cref="LogEntry"/> object and ensures 
        /// all of the properties are set
        /// </summary>
        [TestCategory("LogEntryTests"), TestMethod]
        public void TestLogEntryNormalCtor()
        {
            var le = new LogEntry(LogEventType.Info, "sample message", "sample source", new Exception("sample"));
            
            if (String.IsNullOrEmpty(le.Message) == true)
                Assert.Fail("Message is null or empty");
            if (String.IsNullOrEmpty(le.Source) == true)
                Assert.Fail("Source is null or empty");
            if (le.Exception == null)
                Assert.Fail("Exception is null");
        }

        /// <summary>
        /// Creates a <see cref="LogEntry"/> with a null message
        /// </summary>
        [TestCategory("LogEntryTests"), TestMethod]
        public void TestLogEntryNullMessageCtor()
        {
            var le = new LogEntry(LogEventType.Info, null, "sample source", new Exception("sample"));
            if (le.Message == null)
                Assert.Fail("Message is null");
            if (le.Message != String.Empty)
                Assert.Fail("Message is not empty");
        }

        /// <summary>
        /// Creates a <see cref="LogEntry"/> with a null source
        /// </summary>
        [TestCategory("LogEntryTests"), TestMethod, Ignore]
        public void TestLogEntryNullSourceCtor()
        {
            var le = new LogEntry(LogEventType.Info, "sample message", null, new Exception("sample"));
            if (le.Source == null)
                Assert.Fail("Source is null");
            if(le.Source != String.Empty)
                Assert.Fail("Source is not empty");
        }

        /// <summary>
        /// Creates a <see cref="LogEntry"/> with a null <see cref="Exception"/>
        /// </summary>
        [TestCategory("LogEntryTests"), TestMethod]
        public void TestLogEntryNullExceptionCtor()
        {
            var le = new LogEntry(LogEventType.Info, "sample message", "sample source", null);
            Assert.IsNull(le.Exception);
        }
        #endregion LogEntry Tests

        #region ShutdownLogger Tests
        private LogEntry CreateLogEntry(LogEventType let = LogEventType.Info)
        {
            return new LogEntry(let, "test", null, null);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with an impossible file path
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod, Ignore]
        public void TestShutdownLoggerCtorInvalidFilePath()
        {
            var sl = new ShutdownLogger(@"XYZ:\file.txt");
            sl.Log(CreateLogEntry());
            Assert.Fail("This should've thrown an exception");
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file exists
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerCreateLogFile()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry());
            var logFileExists = File.Exists(logFilePath);
            Assert.IsTrue(logFileExists);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path 
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogInfo()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry());

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = line.ToLower().Contains("test");
            }
            Assert.IsTrue(hasMessage);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogOutput()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Output));

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("output");
            }
            Assert.IsTrue(hasMessage);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogDebug()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Debug));

            //var hasMessage = false;
            //using (var reader = new StreamReader(logFilePath))
            //{
            //    var line = reader.ReadLine();
            //    Console.WriteLine(line);
            //    hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("debug");
            //}
            //Assert.IsTrue(hasMessage);
            Assert.IsFalse(File.Exists(logFilePath));
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogTrace()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Trace));

            //var hasMessage = false;
            //using (var reader = new StreamReader(logFilePath))
            //{
            //    var line = reader.ReadLine();
            //    Console.WriteLine(line);
            //    hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("trace");
            //}
            //Assert.IsTrue(hasMessage);
            Assert.IsFalse(File.Exists(logFilePath));
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogWarning()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Warning));

            //var hasMessage = false;
            //using (var reader = new StreamReader(logFilePath))
            //{
            //    var line = reader.ReadLine();
            //    Console.WriteLine(line);
            //    hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("warning");
            //}
            //Assert.IsTrue(hasMessage);
            Assert.IsFalse(File.Exists(logFilePath));
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogError()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Error));

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("error");
            }
            Assert.IsTrue(hasMessage);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("ShutdownLoggerTests"), TestMethod]
        public void TestShutdownLoggerLogFatal()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(CreateLogEntry(LogEventType.Fatal));

            //var hasMessage = false;
            //using (var reader = new StreamReader(logFilePath))
            //{
            //    var line = reader.ReadLine();
            //    Console.WriteLine(line);
            //    hasMessage = line.ToLower().Contains("test") && line.ToLower().Contains("fatal");
            //}
            //Assert.IsTrue(hasMessage);
            Assert.IsFalse(File.Exists(logFilePath));
        }
        #endregion ShutdownLogger Tests

        #region LogExtensions Tests
        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("LogExtensionTests"), TestMethod]
        public void TestLogExtensionsLogString()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log("message");

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = line.ToLower().Contains("message");
            }
            Assert.IsTrue(hasMessage);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("LogExtensionTests"), TestMethod]
        public void TestLogExtensionsLogNullString()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(message: null);

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = String.IsNullOrEmpty(line) == false;
            }
            Assert.IsTrue(hasMessage);
        }

        /// <summary>
        /// Creates a <see cref="ShutdownLogger"/> with a valid file path
        /// and ensures the log file contains the correct output
        /// </summary>
        [TestCategory("LogExtensionTests"), TestMethod]
        public void TestLogExtensionsLogLogTypeString()
        {
            var sl = new ShutdownLogger(logFilePath);
            sl.Log(LogEventType.Output, "message");

            var hasMessage = false;
            using (var reader = new StreamReader(logFilePath))
            {
                var line = reader.ReadLine();
                Console.WriteLine(line);
                hasMessage = line.ToLower().Contains("message") && line.ToLower().Contains("output");
            }
            Assert.IsTrue(hasMessage);
        }
        #endregion LogExtensions Tests
    }
}
