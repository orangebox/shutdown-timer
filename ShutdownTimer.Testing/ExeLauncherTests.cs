﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Server.ProcessLib;
using System;
using System.IO;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class ExeLauncherTests
    {
        private static string logRootDir;

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            var desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            logRootDir = Path.Combine(desktop, "ExeLauncherTest");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (Directory.Exists(logRootDir) == true)
                Directory.Delete(logRootDir, true);
        }

        [TestCategory("ExeLauncherTests"), TestMethod]
        public void TestValidExe()
        {
            var launcher = new ExeLauncher("ipconfig.exe", null, logRootDir);
            launcher.RunBlock();

            Assert.AreEqual(0, launcher.ExitCode);
        }

        [TestCategory("ExeLauncherTests"), TestMethod]
        public void TestFakeExe()
        {
            try
            {
                var launcher = new ExeLauncher("fakeexe.exe", null, logRootDir);
                launcher.RunBlock();
                Assert.Fail("RunBlock() is supposed to throw an exception");
            }//try
            catch(Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(System.ComponentModel.Win32Exception));
            }
        }

        [TestCategory("ExeLauncherTests"), TestMethod]
        public void TestBadParams()
        {
            var launcher = new ExeLauncher("ipconfig.exe", "asdf", logRootDir);
            var exitCode = launcher.RunBlock();
            Assert.AreNotEqual(0, exitCode);
        }

        [TestCategory("ExeLauncherTests"), TestMethod, Ignore]
        public void TestLogging()
        {
            var launcher = new ExeLauncher("ipconfig.exe", null, logRootDir);
            launcher.RunBlock();

            if (File.Exists(launcher.LogfilePath) == false)
                Assert.Fail("The log file wasn't created");
        }
    }
}
