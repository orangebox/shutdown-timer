﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Utils.Helpers;
using System;
using System.Collections.Generic;
using Cm = System.ComponentModel;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class EnumExtensionTests
    {
        internal enum SampleEnum
        {
            [Cm.Description("one")]
            One,
            [Cm.Description("two")]
            Two,
            [Cm.Description("three")]
            Three,
            //the following don't have descriptions
            Four,
            Five,
            Six
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestGetDescription()
        {
            var actual = SampleEnum.One.GetDescription();
            var expected = "one";
            Assert.AreEqual(expected, actual);
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestInvalidDescription()
        {
            var actual = SampleEnum.Four.GetDescription();
            var expected = "Four";
            Assert.AreEqual(expected, actual);
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestGetValueFromDescription()
        {
            var actual = EnumExtensions.GetValueFromDescription<SampleEnum>("Two");
            var expected = SampleEnum.Two;
            Assert.AreEqual(expected, actual);
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestGetValueFromDescriptionInvalidType()
        {
            try
            {
                EnumExtensions.GetValueFromDescription<int>("Two");
            }
            catch(Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestToDictionary()
        {
            var actual = typeof(SampleEnum).ToDictionary();
            var expected = new Dictionary<string, int>()
            {
                { "one", 0 },
                { "two" , 1 },
                { "three" , 2 },
                { "Four" , 3 },
                { "Five" , 4 },
                { "Six" , 5 },
            };

            foreach (var pair in actual)
            {
                Console.WriteLine($"Key: {pair.Key}, Value: {pair.Value}");
            }

            if (actual.Count != expected.Count)
                Assert.Fail("Sizes aren't equal");
            foreach (var pair in expected)
            {
                if (actual.ContainsKey(pair.Key) == false)
                    Assert.Fail($"Actual dictionary does not contain the following key: \"{pair.Key}\"");
                if (actual.ContainsValue(pair.Value) == false)
                    Assert.Fail($"Actual dictionary does not contain the following value: \"{pair.Value}\"");
            }
        }

        [TestCategory("EnumExtensionTests"), TestMethod]
        public void TestToStringDictionary()
        {
            var actual = typeof(SampleEnum).ToStringDictionary();
            var expected = new Dictionary<string, string>()
            {
                { "one", "One" },
                { "two" , "Two" },
                { "three" , "Three" },
                { "Four" , "Four" },
                { "Five" , "Five" },
                { "Six" , "Six" },
            };

            foreach (var pair in actual)
            {
                Console.WriteLine($"Key: {pair.Key}, Value: {pair.Value}");
            }

            if (actual.Count != expected.Count)
                Assert.Fail("Sizes aren't equal");
            foreach (var pair in expected)
            {
                if (actual.ContainsKey(pair.Key) == false)
                    Assert.Fail($"Actual dictionary does not contain the following key: \"{pair.Key}\"");
                if (actual.ContainsValue(pair.Value) == false)
                    Assert.Fail($"Actual dictionary does not contain the following value: \"{pair.Value}\"");
            }
        }
    }
}
