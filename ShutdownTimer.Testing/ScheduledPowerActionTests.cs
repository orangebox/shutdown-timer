﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Common.Actions;
using System;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class ScheduledPowerActionTests
    {
        #region Constants
        private readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
        private readonly TimeSpan ONE_MINUTE = new TimeSpan(0, 1, 0);
        private readonly TimeSpan ONE_DAY = new TimeSpan(1, 0, 0);
        #endregion Constants

        #region Test methods
        [TestCategory("ScheduledPowerAction"), TestMethod]
        public void TestCtor()
        {
            var ts = ScheduledPowerAction.LOWER_LIMIT.Add(ONE_MINUTE);
            var dt = DateTime.Now.Add(ts);
            var spa = new ScheduledPowerAction(PowerAction.SHUTDOWN, dt);
            Assert.AreEqual(ts, spa.TimeToAction);
        }

        /// <summary>
        /// Creates a <see cref="ScheduledPowerAction"/> object whose <see cref="TimeSpan"/> is zero.
        /// </summary>
        [TestCategory("ScheduledPowerAction"), TestMethod]
        public void TestRunNow()
        {
            RunExceptionSpaTest(DateTime.Now);
        }

        /// <summary>
        /// Creates a <see cref="ScheduledPowerAction"/> object whose <see cref="TimeSpan"/> is below the specified limit.
        /// </summary>
        [TestCategory("ScheduledPowerAction"), TestMethod]
        public void TestBelowLowerLimit()
        {
            DateTime dt;
            if (ScheduledPowerAction.LOWER_LIMIT == ONE_MINUTE)
                dt = DateTime.Now.Add(ONE_SECOND);
            else
            {
                var ts = ScheduledPowerAction.LOWER_LIMIT.Subtract(ONE_MINUTE);
                dt = DateTime.Now.Add(ts);
            }
            RunExceptionSpaTest(dt);
        }

        /// <summary>
        /// Creates a <see cref="ScheduledPowerAction"/> object whose <see cref="TimeSpan"/> is above the specified limit.
        /// </summary>
        [TestCategory("ScheduledPowerAction"), TestMethod]
        public void TestAboveUpperLimit()
        {
            var ts = ScheduledPowerAction.UPPER_LIMIT.Add(ONE_DAY);
            var dt = DateTime.Now.Add(ts);
            RunExceptionSpaTest(dt);
        }

        /// <summary>
        /// Creates a <see cref="ScheduledPowerAction"/> object whose <see cref="TimeSpan"/> is negative (i.e. in the past).
        /// </summary>
        [TestCategory("ScheduledPowerAction"), TestMethod]
        public void TestNegativeTime()
        {
            var dt = DateTime.Now.Subtract(ONE_SECOND);
            RunExceptionSpaTest(dt);
        }
        #endregion Test methods

        #region Private methods
        /// <summary>
        /// Runs a test that is supposed to throw an <see cref="ArgumentException"/>
        /// </summary>
        /// <param name="ts"></param>
        private void RunExceptionSpaTest(TimeSpan ts)
        {
            RunExceptionSpaTest(DateTime.Now.Add(ts));
        }
        /// <summary>
        /// Runs a test that is supposed to throw an <see cref="ArgumentException"/>
        /// </summary>
        /// <param name="dt">The <see cref="DateTime"/> to pass into the <see cref="ScheduledPowerAction"/></param>
        private void RunExceptionSpaTest(DateTime dt)
        {
            try
            {
                var spa = new ScheduledPowerAction(PowerAction.SHUTDOWN, dt);
                Assert.Fail("This was supposed to throw an exception");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }
        }
        #endregion Private methods
    }
}
