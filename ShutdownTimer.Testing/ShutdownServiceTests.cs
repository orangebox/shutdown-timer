﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Common.Actions;
using ShutdownTimer.Wcf;
using System;

namespace ShutdownTimer.Testing
{
    /// <summary>
    /// Summary description for ShutdownServiceTests
    /// </summary>
    [TestClass]
    public class ShutdownServiceTests
    {
        private static ShutdownService svc;
        private static ScheduledPowerAction spa;

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            svc = new ShutdownService();
            spa = new ScheduledPowerAction(PowerAction.SHUTDOWN, DateTime.Now.AddYears(1));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            svc.AbortAction();
        }

        /// <summary>
        /// Passes a <see cref="ScheduledPowerAction"/> to the <see cref="ShutdownService"/> and ensures the
        /// response is true
        /// </summary>
        [TestCategory("ShutdownServiceTests"), TestMethod]
        public void TestScheduleAction()
        {
            var result = svc.ScheduleAction(spa);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Passes a <see cref="ScheduledPowerAction"/> to the <see cref="ShutdownService"/>, then immediately
        /// aborts it and ensures the response is true
        /// </summary>
        [TestCategory("ShutdownServiceTests"), TestMethod]
        public void TestAbort()
        {
            svc.ScheduleAction(spa);
            var result = svc.AbortAction();
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Passes a <see cref="ScheduledPowerAction"/> to the <see cref="ShutdownService"/> and ensures the
        /// service's action is not null
        /// </summary>
        [TestCategory("ShutdownServiceTests"), TestMethod]
        public void TestActionAfterSchedule()
        {
            svc.ScheduleAction(spa);
            Assert.IsNotNull(svc.GetScheduledAction());
        }

        /// <summary>
        /// Passes a <see cref="ScheduledPowerAction"/> to the <see cref="ShutdownService"/>, then immediately
        /// aborts it and ensures the service's action is not null
        /// </summary>
        [TestCategory("ShutdownServiceTests"), TestMethod]
        public void TestNullActionAfterAbort()
        {
            svc.ScheduleAction(spa);
            svc.AbortAction();
            Assert.IsNull(svc.GetScheduledAction());
        }
    }
}
