﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShutdownTimer.Utils.Helpers;
using System;
using System.IO;
using System.Linq;

namespace ShutdownTimer.Testing
{
    [TestClass]
    public class DirectoryHelperTests
    {
        private static readonly string DESKTOP = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        private static string testDir;

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            testDir = Path.Combine(DESKTOP, "test");
            DeleteDirectory();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            DeleteDirectory();
        }

        private static void DeleteDirectory()
        {
            if (Directory.Exists(testDir) == true)
                Directory.Delete(testDir, true);
        }

        [TestCategory("DirectoryHelperTests"), TestMethod]
        public void TestCreateDirectory()
        {
            DirectoryHelpers.CreateDirectoryIfNotExists(testDir);
            Assert.IsTrue(Directory.Exists(testDir));
        }

        [TestCategory("DirectoryHelperTests"), TestMethod]
        public void TestPurgeDirectory()
        {
            if (Directory.Exists(testDir) == false)
                Directory.CreateDirectory(testDir);
            if (Directory.GetFiles(testDir).Any() == false)
            {
                var filename = Path.Combine(testDir, $"{DateTime.Now.Ticks}.txt");
                using (var writer = new StreamWriter(filename))
                {
                    writer.WriteLine("blah blah blah");
                }
            }

            DirectoryHelpers.PurgeDirectory(testDir);
            var hasFiles = Directory.GetFiles(testDir).Any();
            Assert.IsFalse(hasFiles);
        }
    }
}
