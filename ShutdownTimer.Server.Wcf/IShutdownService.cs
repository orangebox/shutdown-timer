﻿using ShutdownTimer.Common.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ShutdownTimer.Wcf
{
    [ServiceContract]
    public interface IShutdownService
    {
        [OperationContract]
        bool IsRunning();

        [OperationContract]
        ScheduledPowerAction GetScheduledAction();

        [OperationContract]
        bool ScheduleAction(ScheduledPowerAction spa);

        [OperationContract]
        bool AbortAction();
    }
}
