﻿using ShutdownTimer.Common.Actions;
using ShutdownTimer.Server.ProcessLib;
using System;

namespace ShutdownTimer.Wcf
{
    public class ShutdownService : IShutdownService
    {
        /// <summary>
        /// Simple check to verify that the service is running.
        /// </summary>
        /// <returns>TRUE</returns>
        public bool IsRunning()
        {
            return true;
        }

        public ScheduledPowerAction GetScheduledAction()
        {
            return ShutdownLauncher.Instance.Action;
        }

        public bool ScheduleAction(ScheduledPowerAction spa)
        {
            try
            {
                ShutdownLauncher.Instance.Run(spa);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool AbortAction()
        {
            try
            {
                ShutdownLauncher.Instance.Abort();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
