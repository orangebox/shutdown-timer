﻿using System;
using System.Collections.Generic;

namespace ShutdownTimer.Client.Backend.Models
{
    public class ExceptionModel
    {
        public Type Type { get; }
        public string Message { get; }
        public string StackTrace { get; }

        public ExceptionModel(Type t, string m, string s)
        {
            Type = t;
            Message = m;
            StackTrace = s;
        }

        public ExceptionModel(Exception ex)
        {
            Type = ex.GetType();
            Message = ex.Message;
            StackTrace = ex.StackTrace;
        }

        public static List<ExceptionModel> ConvertFromList(List<Exception> exList)
        {
            var exceptions = new List<ExceptionModel>();
            foreach (var ex in exList)
            {
                exceptions.Add(new ExceptionModel(ex));
            }
            return exceptions;
        }
    }
}
