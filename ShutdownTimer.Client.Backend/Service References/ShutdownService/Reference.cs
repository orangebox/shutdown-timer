﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This code was auto-generated by Microsoft.VisualStudio.ServiceReference.Platforms, version 14.0.23107.0
// 
namespace ShutdownTimer.Client.Backend.ShutdownService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ShutdownService.IShutdownService")]
    public interface IShutdownService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShutdownService/IsRunning", ReplyAction="http://tempuri.org/IShutdownService/IsRunningResponse")]
        System.Threading.Tasks.Task<bool> IsRunningAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShutdownService/GetScheduledAction", ReplyAction="http://tempuri.org/IShutdownService/GetScheduledActionResponse")]
        System.Threading.Tasks.Task<ShutdownTimer.Common.Actions.ScheduledPowerAction> GetScheduledActionAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShutdownService/ScheduleAction", ReplyAction="http://tempuri.org/IShutdownService/ScheduleActionResponse")]
        System.Threading.Tasks.Task<bool> ScheduleActionAsync(ShutdownTimer.Common.Actions.ScheduledPowerAction spa);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShutdownService/AbortAction", ReplyAction="http://tempuri.org/IShutdownService/AbortActionResponse")]
        System.Threading.Tasks.Task<bool> AbortActionAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IShutdownServiceChannel : ShutdownTimer.Client.Backend.ShutdownService.IShutdownService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ShutdownServiceClient : System.ServiceModel.ClientBase<ShutdownTimer.Client.Backend.ShutdownService.IShutdownService>, ShutdownTimer.Client.Backend.ShutdownService.IShutdownService {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public ShutdownServiceClient() : 
                base(ShutdownServiceClient.GetDefaultBinding(), ShutdownServiceClient.GetDefaultEndpointAddress()) {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IShutdownService.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ShutdownServiceClient(EndpointConfiguration endpointConfiguration) : 
                base(ShutdownServiceClient.GetBindingForEndpoint(endpointConfiguration), ShutdownServiceClient.GetEndpointAddress(endpointConfiguration)) {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ShutdownServiceClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(ShutdownServiceClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress)) {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ShutdownServiceClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(ShutdownServiceClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress) {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ShutdownServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Threading.Tasks.Task<bool> IsRunningAsync() {
            return base.Channel.IsRunningAsync();
        }
        
        public System.Threading.Tasks.Task<ShutdownTimer.Common.Actions.ScheduledPowerAction> GetScheduledActionAsync() {
            return base.Channel.GetScheduledActionAsync();
        }
        
        public System.Threading.Tasks.Task<bool> ScheduleActionAsync(ShutdownTimer.Common.Actions.ScheduledPowerAction spa) {
            return base.Channel.ScheduleActionAsync(spa);
        }
        
        public System.Threading.Tasks.Task<bool> AbortActionAsync() {
            return base.Channel.AbortActionAsync();
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync() {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync() {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration) {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IShutdownService)) {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration) {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IShutdownService)) {
                return new System.ServiceModel.EndpointAddress("http://localhost:8733/ShutdownService");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding() {
            return ShutdownServiceClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IShutdownService);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress() {
            return ShutdownServiceClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IShutdownService);
        }
        
        public enum EndpointConfiguration {
            
            BasicHttpBinding_IShutdownService,
        }
    }
}
