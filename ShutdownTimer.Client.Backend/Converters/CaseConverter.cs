﻿using System;
using System.Globalization;
using Windows.UI.Xaml.Data;

namespace ShutdownTimer.Client.Backend.Converters
{
    public class CaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                switch ((parameter as string).ToLower())
                {
                    case "upper":
                    case "u":
                        return CultureInfo.CurrentCulture.TextInfo.ToUpper(value as string);
                    case "lower":
                    case "l":
                        return CultureInfo.CurrentCulture.TextInfo.ToLower(value as string);
                    //case "word":
                    //case "w":
                    //case "title":
                    //case "t":
                    //    return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value as string);
                    default:
                        throw new NotImplementedException($"Unhandled parameter: {parameter}");
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
