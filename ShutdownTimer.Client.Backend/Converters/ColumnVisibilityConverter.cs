﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ShutdownTimer.Client.Backend.Converters
{
    public class ColumnVisibilityConverter : IValueConverter
    {
        private readonly GridLength VISIBLE = new GridLength(1, GridUnitType.Star);
        private readonly GridLength HIDDEN = new GridLength(0, GridUnitType.Pixel);

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                var isVisible = (bool)value;
                switch ((parameter as string).ToLower())
                {
                    case "h":
                        return isVisible == true ? VISIBLE : HIDDEN;
                    case "d":
                        return isVisible == true ? VISIBLE : HIDDEN;
                    case "m":
                        return isVisible == true ? VISIBLE : HIDDEN;
                    default:
                        throw new NotImplementedException($"Unhandled parameter: \"{parameter as string}\"");
                }//switch
            }//try
            catch (Exception)
            {
                return VISIBLE;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
