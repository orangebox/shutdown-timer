﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ShutdownTimer.Client.Backend.Converters
{
    public class DurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                var ts = (TimeSpan)value;
                return new Duration(ts);
            }//try
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"{ex.GetType().Name} caught");
                System.Diagnostics.Debug.WriteLine($"Message: {ex.Message}");
                System.Diagnostics.Debug.WriteLine($"Stack Trace:\n{ex.StackTrace}");
                return new Duration(TimeSpan.Zero);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
