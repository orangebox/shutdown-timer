﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ShutdownTimer.Client.Backend.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            switch (parameter as string)
            {
                case "negate":
                case "n":
                    return ((bool)value) == false ? Visibility.Visible : Visibility.Collapsed;
                default:
                    return ((bool)value) == true ? Visibility.Visible : Visibility.Collapsed;
            }//switch
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            switch (parameter as string)
            {
                case "negate":
                case "n":
                    return ((Visibility)value) != Visibility.Visible;
                default:
                    return ((Visibility)value) == Visibility.Visible;
            }//switch
        }
    }
}
