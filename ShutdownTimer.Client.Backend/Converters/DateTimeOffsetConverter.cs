﻿using ShutdownTimer.Common.Helpers;
using System;
using Windows.UI.Xaml.Data;

namespace ShutdownTimer.Client.Backend.Converters
{
    public class DateTimeOffsetConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                var dt = (DateTime)value;
                return new DateTimeOffset(dt);
            }
            catch(Exception ex)
            {
                ex.Flatten().Print();
                return DateTimeOffset.MinValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            try
            {
                var dto = (DateTimeOffset)value;
                return dto.DateTime;
            }
            catch(Exception ex)
            {
                ex.Flatten().Print();
                return DateTime.MinValue;
            }
        }
    }
}
