﻿using ShutdownTimer.Client.Backend.ShutdownService;

namespace ShutdownTimer.Client.Backend
{
    public static class App
    {
        public static readonly ShutdownServiceClient Client = new ShutdownServiceClient();
    }
}
