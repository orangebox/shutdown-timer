﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using ShutdownTimer.Client.Backend.Data;
using ShutdownTimer.Client.Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

namespace ShutdownTimer.Client.Backend.ViewModels
{
    public class ExceptionViewModel : ViewModelBase
    {
        #region Variables
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;
        private readonly INavigationService navService;
        private readonly DataPackage dataPackage;
        #endregion Variables

        #region Binding Variables
        private List<ExceptionModel> exceptions;
        public List<ExceptionModel> Exceptions
        {
            get { return exceptions; }
            set { Set(nameof(Exceptions), ref exceptions, value); }
        }
        #endregion Binding Variables

        #region Commands
        public RelayCommand CopyCommand { get; }
        public RelayCommand<ExceptionModel> CopySelectedCommand { get; }
        #endregion Commands

        #region Constructors
        public ExceptionViewModel(IDataService d, IDialogService dlg, INavigationService n)
        {
            dataService = d;
            dialogService = dlg;
            navService = n;

            Exceptions = dataService.GetExceptions();

            if(IsInDesignMode == false)
            {
                dataPackage = new DataPackage();
                dataPackage.RequestedOperation = DataPackageOperation.Copy;
                CopyCommand = new RelayCommand(CopyExecuted, CopyCanExecute);
                CopySelectedCommand = new RelayCommand<ExceptionModel>(CopySelectedExecuted);
            }
        }
        #endregion Constructors

        #region Private methods
        private void CopyExceptions(params ExceptionModel[] exceptions)
        {
            var copyBuilder = new StringBuilder();
            var hasMultiple = exceptions.Length > 1;

            if (hasMultiple == true)
                copyBuilder.AppendLine($"{exceptions.Length.ToString("N0")} exceptions caught\n");
            var index = 0;
            foreach (var exception in exceptions)
            {
                if (hasMultiple == true)
                    copyBuilder.Append($"#{++index} - ");
                copyBuilder.AppendLine($"Type: {exception.Type.Name}");
                copyBuilder.AppendLine($"Message: {exception.Message}");
                copyBuilder.AppendLine($"StackTrace:\n{exception.StackTrace}");
                if (hasMultiple == true)
                    copyBuilder.AppendLine("------------------------------------");
            }

            dataPackage.SetText(copyBuilder.ToString());
            Clipboard.SetContent(dataPackage);
        }
        #endregion Private methods

        #region Commands CanExecute
        private bool CopyCanExecute()
        {
            return Exceptions.Count > 1;
        }
        #endregion Commands CanExecute

        #region Commands Executed
        private void CopyExecuted()
        {
            CopyExceptions(Exceptions.ToArray());
        }

        private void CopySelectedExecuted(ExceptionModel ex)
        {
            CopyExceptions(ex);
        }
        #endregion Commands Executed
    }
}
