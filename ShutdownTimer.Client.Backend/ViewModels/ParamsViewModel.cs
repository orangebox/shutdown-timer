﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using ShutdownTimer.Client.Backend.Data;
using ShutdownTimer.Common.Actions;
using ShutdownTimer.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShutdownTimer.Client.Backend.ViewModels
{
    public class ParamsViewModel : ViewModelBase
    {
        #region Variables
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;
        private readonly INavigationService navService;
        private readonly TimeSpan PROMPT_LIMIT = new TimeSpan(0, 1, 0);
        #endregion Variables

        #region Binding Variables
        private bool isConnecting;
        private PowerAction selectedAction;
        private DateTime scheduleDt;

        public bool IsConnecting
        {
            get { return isConnecting; }
            set { Set(nameof(IsConnecting), ref isConnecting, value); }
        }
        public PowerAction SelectedAction
        {
            get { return selectedAction; }
            set
            {
                var hasSet = Set(nameof(SelectedAction), ref selectedAction, value);
                if (hasSet == true)
                    SubmitCommand.RaiseCanExecuteChanged();
            }
        }
        public DateTime ScheduleDt
        {
            get { return scheduleDt; }
            set
            {
                var hasSet = Set(nameof(ScheduleDt), ref scheduleDt, value);
                if (hasSet == true)
                    SubmitCommand.RaiseCanExecuteChanged();
            }
        }
        public TimeSpan TimeProxy
        {
            get { return ScheduleDt - ScheduleDt.Date; }
            set
            {
                if (TimeProxy != value)
                {
                    ScheduleDt = ScheduleDt.Date.Add(value);
                    RaisePropertyChanged(nameof(TimeProxy));
                }
            }
        }
        public List<PowerAction> Actions { get { return PowerAction.ALL_ACTIONS; } }
        #endregion Binding Variables

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand SubmitCommand { get; private set; }
        public RelayCommand ResetCommand { get; private set; }
        #endregion Commands

        #region Constructors
        public ParamsViewModel(IDataService d, IDialogService dlg, INavigationService n)
        {
            LoadedCommand = new RelayCommand(LoadedExecuted);
            SubmitCommand = new RelayCommand(SubmitExecuted, SubmitCanExecute);
            ResetCommand = new RelayCommand(ResetExecuted);

            dataService = d;
            dialogService = dlg;
            navService = n;

            isConnecting = true;

            if (IsInDesignMode == true)
                InitializeVariables();
        }
        #endregion Constructors

        #region Private methods
        private void InitializeVariables()
        {
            SelectedAction = dataService.GetSelectedAction();
            ScheduleDt = dataService.GetScheduledDateTime().Trim(TimeSpan.TicksPerMinute);
            RaisePropertyChanged(nameof(TimeProxy));
        }//private void InitializeVariables()
        #endregion Private methods

        #region Commands CanExecute
        private bool SubmitCanExecute()
        {
            var hasSelectedAction = SelectedAction != null;
            var hasSetTime = ScheduleDt != null && DateTime.Now < ScheduleDt;
            return hasSelectedAction == true && hasSetTime == true;
        }
        #endregion Commands CanExecute

        #region Commands Executed
        private async void LoadedExecuted()
        {
            InitializeVariables();

            IsConnecting = true;
            var spa = await Backend.App.Client.GetScheduledActionAsync();
            if (spa != null)
                navService.NavigateTo(typeof(TimerViewModel).Name, spa);
            IsConnecting = false;
        }

        private async void SubmitExecuted()
        {
            try
            {
                var spa = new ScheduledPowerAction(SelectedAction, ScheduleDt);
                var isSure = true;
                if (spa.TimeToAction <= PROMPT_LIMIT)
                    isSure = await dialogService.ShowMessage($"This event is scheduled less than {PROMPT_LIMIT.TotalSeconds.ToString("N0")} seconds from now.\n\nAre you sure you want to schedule it so soon?", "Warning", "Yes", "No", null);
                if (isSure == true && spa.Name == PowerAction.RESTART_ADVANCED.Name)
                    isSure = await dialogService.ShowMessage("This will restart your computer and display the advanced boot options menu, which allows you to permanently wipe out the data on your computer.\n\nAre you sure you want to continue?", "Warning", "Yes", "No", null);
                if (isSure == true)
                {
                    var serviceResponse = await Task.Run(() => App.Client.ScheduleActionAsync(spa));
                    if (serviceResponse == true)
                        navService.NavigateTo(typeof(TimerViewModel).Name, spa);
                    else
                        await dialogService.ShowError("Unable to schedule the power action.", "Something went wrong :(", "OK", null);
                }//if
            }//try
            catch (ArgumentException aoore)
            {
                await dialogService.ShowError(aoore.Message, "Unable to schedule the event", "OK", null);
            }//catch
        }

        private void ResetExecuted()
        {
            InitializeVariables();
        }
        #endregion Commands Executed
    }
}
