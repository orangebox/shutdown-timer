﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using ShutdownTimer.Client.Backend.Data;
using ShutdownTimer.Common.Helpers;
using System;
using System.Text;

namespace ShutdownTimer.Client.Backend.ViewModels
{
    public class PingViewModel : ViewModelBase
    {
        #region Variables
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;
        private readonly INavigationService navService;
        #endregion Variables

        #region Binding Variables
        private bool isPinging;
        private int port;
        private string stepsText;

        public bool IsPinging
        {
            get { return isPinging; }
            set
            {
                var hasSet = Set(nameof(IsPinging), ref isPinging, value);
                if (hasSet == true && PingCommand != null)
                    PingCommand.RaiseCanExecuteChanged();
            }//set
        }
        public int Port
        {
            get { return port; }
            set { Set(nameof(Port), ref port, value); }
        }
        public string StepsText
        {
            get { return stepsText; }
            set { Set(nameof(StepsText), ref stepsText, value); }
        }
        public string CheckingText { get { return "Checking if the service is running..."; } }
        public string ErrorText { get { return "It seems as if the service is not running.\nCheck the following and try again."; } }
        #endregion Binding Variables

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand PingCommand { get; private set; }
        #endregion Commands

        #region Constructors
        public PingViewModel(IDataService d, IDialogService dlg, INavigationService n)
        {
            dataService = d;
            dialogService = dlg;
            navService = n;
            IsPinging = !IsInDesignMode;
            Port = dataService.GetServicePort();
            SetErrorState();

            if (IsInDesignMode == false)
            {
                LoadedCommand = new RelayCommand(LoadedExecuted);
                PingCommand = new RelayCommand(PingExecuted, PingCanExecute);
            }//if
        }//public PingViewModel(IDataService d, IDialogService dlg, INavigationService n)
        #endregion Constructors

        #region Private methods
        private async void PingService()
        {
            var dispatcher = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher;
            await dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                IsPinging = true;
                try
                {
                    var result = await App.Client.IsRunningAsync();
                    if (result == true)
                        navService.NavigateTo(typeof(ParamsViewModel).Name);
                    //else
                    //    SetErrorState();
                }//try
                catch (Exception ex)
                {
                    ex.Flatten().Print();
                    //SetErrorState();
                }//catch
                IsPinging = false;
            });
        }//private async void PingService()

        private void SetErrorState()
        {
            var stepsBuilder = new StringBuilder();
            stepsBuilder.AppendLine($"Ensure port {Port} is open in Windows Firewall");
            stepsBuilder.AppendLine("Ensure the service is running in the Task Manager");
            stepsBuilder.AppendLine("Ensure the service is installed");
            stepsBuilder.AppendLine("If it's not installed, go to our website (link below) to download the installer");
            StepsText = stepsBuilder.ToString();
        }
        #endregion Private methods

        #region Commands CanExecute
        private bool PingCanExecute()
        {
            return IsPinging == false;
        }//private bool PingCanExecute()
        #endregion Commands CanExecute

        #region Commands Executed
        private async void LoadedExecuted()
        {
            var msg = "We lost the connection to the service. Follow the steps in the next page to try and get the service back up.";
            await dialogService.ShowError(msg, "Something went wrong :(", "OK", null);
            PingService();
        }

        private void PingExecuted()
        {
            PingService();
        }
        #endregion Commands Executed
    }
}
