﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using ShutdownTimer.Client.Backend.Data;
using ShutdownTimer.Common.Actions;
using System;
using System.Threading.Tasks;

namespace ShutdownTimer.Client.Backend.ViewModels
{
    public class TimerViewModel : ViewModelBase
    {
        #region Variables
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;
        private readonly INavigationService navService;
        private readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
        private bool continueLooping;
        #endregion Variables

        #region Binding Variables
        private ScheduledPowerAction scheduledAction;
        private TimeSpan remainingTime;
        private bool isDaysVisible;
        private bool isHoursVisible;
        private bool isMinutesVisible;
        private int initialDays;

        public string InfoText { get { return "NOTE: Closing this app will NOT abort the scheduled action. If you want to abort, click the Abort button."; } }
        public ScheduledPowerAction ScheduledAction
        {
            get { return scheduledAction; }
            set { Set(nameof(ScheduledAction), ref scheduledAction, value); }
        }
        public TimeSpan RemainingTime
        {
            get { return remainingTime; }
            set { Set(nameof(RemainingTime), ref remainingTime, value); }
        }
        public bool IsDaysVisible
        {
            get { return isDaysVisible; }
            set { Set(nameof(IsDaysVisible), ref isDaysVisible, value); }
        }
        public bool IsHoursVisible
        {
            get { return isHoursVisible; }
            set { Set(nameof(IsHoursVisible), ref isHoursVisible, value); }
        }
        public bool IsMinutesVisible
        {
            get { return isMinutesVisible; }
            set { Set(nameof(IsMinutesVisible), ref isMinutesVisible, value); }
        }
        public int InitialDays
        {
            get { return initialDays; }
            set { Set(nameof(InitialDays), ref initialDays, value); }
        }
        #endregion Binding Variables

        #region Commands
        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand AbortCommand { get; private set; }
        #endregion Commands

        #region Constructors
        public TimerViewModel(IDataService d, IDialogService dlg, INavigationService n)
        {
            LoadedCommand = new RelayCommand(LoadedExecuted);
            AbortCommand = new RelayCommand(AbortExecuted);

            dataService = d;
            dialogService = dlg;
            navService = n;
            InitializeVariables();
        }
        #endregion Constructors

        #region Private methods
        private void InitializeVariables()
        {
            if (IsInDesignMode == true)
                ScheduledAction = new ScheduledPowerAction(dataService.GetSelectedAction(), dataService.GetScheduledDateTime());
            continueLooping = true;
            RemainingTime = (ScheduledAction?.TimeOfAction ?? DateTime.MaxValue) - DateTime.Now;
            InitialDays = RemainingTime.Days;
            SetVisibilities();
        }

        private void SetVisibilities()
        {
            IsDaysVisible = !(RemainingTime.Days == 0);
            IsHoursVisible = !(RemainingTime.Days == 0 && RemainingTime.Hours == 0);
            IsMinutesVisible = !(RemainingTime.Days == 0 && RemainingTime.Hours == 0 && RemainingTime.Minutes == 0);
        }
        #endregion Private methods

        #region Commands CanExecute
        #endregion Commands CanExecute

        #region Commands Executed
        private async void LoadedExecuted()
        {
            InitializeVariables();
            var dispatcher = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher;
            await dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                while (RemainingTime.TotalSeconds > 0 && continueLooping == true)
                {
                    await Task.Delay(ONE_SECOND);
                    RemainingTime = ScheduledAction.TimeOfAction - DateTime.Now;
                    SetVisibilities();
                }//while
#if DEBUG
                if (continueLooping == true)
                {
                    await dialogService.ShowMessage($"This is when the \"{ScheduledAction.Name}\" action would execute.", "Time's up!");
                    navService.NavigateTo(typeof(ParamsViewModel).Name);
                }//if
#endif
            });
        }

        private async void AbortExecuted()
        {
            continueLooping = false;
            var response = await App.Client.AbortActionAsync();
            if (response == false)
                await dialogService.ShowError("Unable to submit the abort command.", "Something went wrong :(", "OK", null);
            navService.NavigateTo(typeof(ParamsViewModel).Name);
        }
        #endregion Commands Executed
    }
}