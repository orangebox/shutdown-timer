﻿using ShutdownTimer.Client.Backend.Models;
using ShutdownTimer.Common.Actions;
using System;
using System.Collections.Generic;

namespace ShutdownTimer.Client.Backend.Data
{
    public class DesignDataService : IDataService
    {
        public string GetPingStatusText()
        {
            return "Sample ping status text";
        }

        public DateTime GetScheduledDateTime()
        {
            return DateTime.Now.Add(new TimeSpan(99, 99, 99, 99));
        }

        public PowerAction GetSelectedAction()
        {
            return PowerAction.RESTART_ADVANCED;
        }

        public int GetServicePort()
        {
            return 12345;
        }

        public List<ExceptionModel> GetExceptions()
        {
            var exceptions = new List<ExceptionModel>();
            for (int index = 0; index < 10; index++)
            {
                var type = typeof(NotImplementedException);
                var message = $"This is exception {index + 1}";
                var stackTrace = "This is a sample stack trace\nThis is a sample stack trace\nThis is a sample stack trace";
                exceptions.Add(new ExceptionModel(type, message, stackTrace));
            }
            return exceptions;
        }
    }
}
