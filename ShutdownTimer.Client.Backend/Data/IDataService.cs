﻿using ShutdownTimer.Client.Backend.Models;
using ShutdownTimer.Common.Actions;
using System;
using System.Collections.Generic;

namespace ShutdownTimer.Client.Backend.Data
{
    public interface IDataService
    {
        string GetPingStatusText();
        PowerAction GetSelectedAction();
        DateTime GetScheduledDateTime();
        int GetServicePort();
        List<ExceptionModel> GetExceptions();
    }
}
