﻿using ShutdownTimer.Client.Backend.Models;
using ShutdownTimer.Common.Actions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShutdownTimer.Client.Backend.Data
{
    public class DataService : IDataService
    {
        public string GetPingStatusText()
        {
            return String.Empty;
        }

        public DateTime GetScheduledDateTime()
        {
            return DateTime.Now.Add(ScheduledPowerAction.LOWER_LIMIT);
        }

        public PowerAction GetSelectedAction()
        {
            return PowerAction.ALL_ACTIONS.First();
        }

        public int GetServicePort()
        {
            return App.Client.ChannelFactory.Endpoint.Address.Uri.Port;
        }

        public List<ExceptionModel> GetExceptions()
        {
            return new List<ExceptionModel>();
        }
    }
}
