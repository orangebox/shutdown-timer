﻿using ShutdownTimer.Common.Actions;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ShutdownTimer.Server.ProcessLib
{
    public class ShutdownLauncher
    {
        #region Singleton implementation
        private readonly string LOG_ROOT_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "SDT");
        private static Lazy<ShutdownLauncher> lazyLauncher = new Lazy<ShutdownLauncher>(() => new ShutdownLauncher());
        public static ShutdownLauncher Instance { get { return lazyLauncher.Value; } }
        #endregion Singleton implementation

        #region Variables
        private const string _executable = "shutdown.exe";
        private bool continueLooping = true;
        public ScheduledPowerAction Action { get; private set; }
        #endregion Variables

        #region Constructors
        private ShutdownLauncher()
        {

        }
        #endregion Constructors

        #region Private methods
        private async void Loop()
        {
            while (continueLooping == true && Action.TimeToAction > TimeSpan.Zero)
            {
                await Task.Delay(1000);
            }//while
            if (continueLooping == true)
            {
#if !DEBUG
                var exitCode = new ExeLauncher(_executable, Action.Argument, LOG_ROOT_PATH).RunBlock();
#else
                Abort();
#endif
            }
        }
        #endregion Private methods

        #region Public methods
        public void Run(ScheduledPowerAction spa)
        {
            continueLooping = true;
            Action = spa;
            Loop();
        }

        public void Abort()
        {
            continueLooping = false;
            Action = null;
        }
        #endregion Public methods
    }
}
