﻿//using ShutdownTimer.Utils.Logging;
using System;
using System.Diagnostics;
using System.IO;

namespace ShutdownTimer.Server.ProcessLib
{
    public class ExeLauncher
    {
        #region Variables
        public readonly string ExeFilename;
        public readonly string Arguments;
        public readonly string WorkingDirectory;
        //public ShutdownLogger Logger;

        public int ExitCode { get; private set; }
        public string LogfilePath { get; private set; }
        #endregion Variables

        #region Constructors
        public ExeLauncher(string exe, string args, string logRootPath, string workingDir = "")
        {
            this.ExeFilename = exe;
            this.Arguments = args;
            if (String.IsNullOrEmpty(workingDir) == true)
                this.WorkingDirectory = Path.GetDirectoryName(this.ExeFilename);
            else
                this.WorkingDirectory = workingDir;

            //var rootDir = Path.Combine(logRootPath, DateTime.Now.ToString("yyyy-MM-dd"));
            //ShutdownTimer.Utils.Helpers.DirectoryHelpers.CreateDirectoryIfNotExists(rootDir);
            //var filename = $"{Path.GetFileNameWithoutExtension(this.ExeFilename)}-{DateTime.Now.ToString("HH-mm-ss-ffff")}.txt";
            //this.LogfilePath = Path.Combine(rootDir, filename);

            //Logger = new ShutdownLogger(this.LogfilePath);
        }
        #endregion Constructors

        #region Private methods
        private Process CreateProcessObject()
        {
            var process = new Process();
            process.StartInfo.FileName = this.ExeFilename;
            process.StartInfo.Arguments = this.Arguments;
            process.StartInfo.WorkingDirectory = this.WorkingDirectory;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            //process.OutputDataReceived += (s, e) => {
            //    if(!String.IsNullOrEmpty(e.Data))
            //        Logger.Log(LogEventType.Output, e.Data);
            //};
            //process.ErrorDataReceived += (s, e) => {
            //    if(!String.IsNullOrEmpty(e.Data))
            //        Logger.Log(LogEventType.Output, e.Data);
            //};

#if DEBUG
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
#else
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
#endif
            //Logger.Log($"Launching {this.ExeFilename} with args\n{this.Arguments}\n-------");
            return process;
        }
        #endregion Private methods

        #region Public methods
        public int RunBlock(int waitTime = 0)
        {
            using (var process = CreateProcessObject())
            {
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                if (waitTime != 0)
                    process.WaitForExit(waitTime);
                else
                    process.WaitForExit();

                process.CancelOutputRead();
                process.CancelErrorRead();

                this.ExitCode = process.ExitCode;
                //Logger.Log($"Exiting with exit code {this.ExitCode}");
            }//using
            return this.ExitCode;
        }

        public override string ToString()
        {
            return $"Process: \"{ExeFilename}\", {Arguments}";
        }
        #endregion Public methods
    }
}
