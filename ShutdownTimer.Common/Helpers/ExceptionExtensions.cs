﻿using System;
using System.Collections.Generic;

namespace ShutdownTimer.Common.Helpers
{
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Turn an <see cref="Exception"/>'s InnerExceptions into a collection of <see cref="Exception"/>s
        /// </summary>
        /// <param name="ex"></param>
        /// <returns>A collection of <see cref="Exception"/>s</returns>
        public static List<Exception> Flatten(this Exception ex)
        {
            var exceptionList = new List<Exception>();
            var currentException = ex;
            do
            {
                exceptionList.Add(currentException);
                currentException = currentException.InnerException;
            } while (currentException != null);
            return exceptionList;
        }

        public static void Print(this List<Exception> exceptions)
        {
            for (int index = 0; index < exceptions.Count; index++)
            {
                var exception = exceptions[index];

                System.Diagnostics.Debug.WriteLine($"Exception {index + 1}: {exception.GetType().Name} caught");
                System.Diagnostics.Debug.WriteLine($"Message: {exception.Message}");
                System.Diagnostics.Debug.WriteLine($"Stack Trace:\n{exception.StackTrace}");
            }//for
        }//public static void Print(this List<Exception> exceptions)
    }
}
