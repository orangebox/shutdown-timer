﻿using System;

namespace ShutdownTimer.Common.Helpers
{
    public static class DateTimeExtensions
    {
        public static DateTime Trim(this DateTime dt, long roundTicks)
        {
            return new DateTime(dt.Ticks - dt.Ticks % roundTicks);
        }
    }
}
