﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ShutdownTimer.Common.Actions
{
    [DataContract]
    public class PowerAction
    {
        #region Constants
        /// <summary>Shutdown the computer</summary>
        public static readonly PowerAction SHUTDOWN = new PowerAction("Shut down", "/s /f /t 0", "Shuts down the computer");
        /// <summary>Full shutdown and restart the computer</summary>
        public static readonly PowerAction RESTART = new PowerAction("Restart", "/r /f /t 0", "Restarts the computer");
        /// <summary>Full shutdown and restart the computer. After the system is rebooted, restart any registered applications.</summary>
        public static readonly PowerAction RESTART_REOPEN = new PowerAction("Restart and reopen apps", "/g /f /t 0", "Restarts the computer and reopens any registered apps");
        /// <summary>Hibernate the local computer</summary>
        public static readonly PowerAction HIBERNATE = new PowerAction("Hibernate", "/h", "Hibernates the computer");
        /// <summary>Log off. This cannot be used with the /m or /d options.</summary>
        public static readonly PowerAction LOGOFF = new PowerAction("Log off", "/l", "Logs the user off");
        /// <summary>Performs a shutdown of the computer and prepares it for fast startup. Must be used with the /s option.</summary>
        public static readonly PowerAction HYBRID = new PowerAction("Hybrid", "/s /hybrid /f /t 0", "Shuts the computer down and prepares it for fast startup");
        /// <summary>Go to the advanced boot options menu and restart the computer. Must be used with the /r option.</summary>
        public static readonly PowerAction RESTART_ADVANCED = new PowerAction("Open Advanced Boot Options", "/r /o /f /t 0", "Restarts the computer and loads the advanced boot options menu");
        
        /*************************************************************
         * Sleep: rundll32.exe powrprof.dll,SetSuspendState 0,1,0    *
         * Lock Workstation: rundll32.exe User32.dll,LockWorkStation *
         *************************************************************/

        public static readonly List<PowerAction> ALL_ACTIONS = new List<PowerAction>()
        {
            SHUTDOWN,
            RESTART,
            //RESTART_REOPEN,
            //RESTART_ADVANCED,
            HIBERNATE,
            LOGOFF,
            //HYBRID,
        };
        #endregion Constants

        #region Properties
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public string Argument { get; private set; }
        [DataMember]
        public string Description { get; private set; }
        #endregion Properties

        #region Constructors
        protected PowerAction(string n, string a, string d)
        {
            Name = n;
            Argument = a;
            Description = d;
        }
        #endregion Constructors
    }
}
