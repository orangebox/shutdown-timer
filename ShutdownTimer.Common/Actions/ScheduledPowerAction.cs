﻿using System;
using System.Runtime.Serialization;
using ShutdownTimer.Common.Helpers;

namespace ShutdownTimer.Common.Actions
{
    [DataContract]
    public class ScheduledPowerAction : PowerAction
    {
        public static readonly TimeSpan LOWER_LIMIT = new TimeSpan(0, 1, 0);
        public static readonly TimeSpan UPPER_LIMIT = new TimeSpan(0, 0, 0, 315360000);

        [DataMember]
        public DateTime TimeOfAction { get; private set; }
        public TimeSpan TimeToAction { get { return TimeOfAction - DateTime.Now.Trim(TimeSpan.TicksPerMinute); } }

        public ScheduledPowerAction(PowerAction pa, DateTime dt)
            : base(pa.Name, pa.Argument, pa.Description)
        {
            TimeOfAction = dt.Trim(TimeSpan.TicksPerMinute);
            //if (TimeToAction < LOWER_LIMIT)
            //    TimeOfAction = DateTime.Now.Add(LOWER_LIMIT);
            /*else*/ if (TimeToAction < LOWER_LIMIT || TimeToAction > UPPER_LIMIT)
            {
                var lower = DateTime.Now.Add(LOWER_LIMIT);
                var upper = DateTime.Now.Add(UPPER_LIMIT);
                throw new ArgumentException($"The valid time range is from {lower} to {upper}");
            }
        }
    }
}