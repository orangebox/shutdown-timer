﻿using System.ServiceProcess;
using System.ServiceModel;
using ShutdownTimer.Wcf;

namespace ShutdownTimer.Server.Host
{
    public partial class ShutdownWinService : ServiceBase
    {
        internal static ServiceHost hostShutdownTimer = null;
        public ShutdownWinService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if(hostShutdownTimer != null)
            {
                hostShutdownTimer.Close();
            }
            hostShutdownTimer = new ServiceHost(typeof(ShutdownService));
            hostShutdownTimer.Open();
        }

        protected override void OnStop()
        {
            if (hostShutdownTimer != null)
            {
                hostShutdownTimer.Close();
            }
        }
    }
}
