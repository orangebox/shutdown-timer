﻿using System.ServiceProcess;

namespace ShutdownTimer.Server.Host
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ShutdownWinService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
