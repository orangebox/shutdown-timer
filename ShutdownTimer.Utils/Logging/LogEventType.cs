﻿using System.ComponentModel;

namespace ShutdownTimer.Utils.Logging
{
    public enum LogEventType
    {
        [Description("Info")]
        Info,
        [Description("Output")]
        Output,
        [Description("Debug")]
        Debug,
        [Description("Trace")]
        Trace,
        [Description("Warning")]
        Warning,
        [Description("Error")]
        Error,
        [Description("Fatal")]
        Fatal
    }
}
