﻿using ShutdownTimer.Utils.Helpers;
using System;
using NLog;
using NLog.Targets;
using NLog.Config;

namespace ShutdownTimer.Utils.Logging
{
    public class ShutdownLogger : ILogger
    {
        private string _filePath;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public ShutdownLogger(string filePath)
        {
            _filePath = filePath;
            var config = new LoggingConfiguration();
            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);
            fileTarget.FileName = filePath;
            fileTarget.Layout = "${shortDate} ${date:format=hh\\:mm\\:ss tt} ${message}";
            var infoRule = new LoggingRule("*", LogLevel.Info, fileTarget);
            config.LoggingRules.Add(infoRule);
            var errorRule = new LoggingRule("*", LogLevel.Error, fileTarget);
            config.LoggingRules.Add(errorRule);
            LogManager.Configuration = config;
        }

        public void Log(LogEntry entry)
        {
            switch (entry.EventType)
            {
                case LogEventType.Info:
                    _logger.Info(entry.Message);
                    break;
                case LogEventType.Output:
                    _logger.Info($"{LogEventType.Output.GetDescription()}: {entry.Message}");
                    break;
                case LogEventType.Debug:
                    break;
                case LogEventType.Trace:
                    break;
                case LogEventType.Warning:
                    break;
                case LogEventType.Error:
                    _logger.Error($"{LogEventType.Error.GetDescription()}: {entry.Message}");
                    break;
                case LogEventType.Fatal:
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"\"{entry.EventType}\" is not a handled LogEventType");
            }
        }
    }
}
