﻿namespace ShutdownTimer.Utils.Logging
{
    public static class LogExtenstions
    {
        public static void Log(this ILogger logger, string message)
        {
            logger.Log(new LogEntry(LogEventType.Info, message, null, null));
        }

        public static void Log(this ILogger logger, LogEventType severity, string message)
        {
            logger.Log(new LogEntry(severity, message, null, null));
        }
    }
}
