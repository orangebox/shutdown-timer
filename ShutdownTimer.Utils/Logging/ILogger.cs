﻿namespace ShutdownTimer.Utils.Logging
{
    public interface ILogger
    {
        void Log(LogEntry entry);
    }
}
