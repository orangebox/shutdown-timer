﻿using System;

namespace ShutdownTimer.Utils.Logging
{
    public class LogEntry
    {
        public LogEventType EventType { get; private set; }
        public string Message { get; private set; }
        public string Source { get; private set; }
        public Exception Exception { get; private set; }

        public LogEntry(LogEventType eventType, string message, string source, Exception exception)
        {
            //if (message == null)
            //    throw new NullReferenceException("Message cannot be null");
            //if (String.IsNullOrWhiteSpace(message))
            //    throw new ArgumentException("Message cannot be blank");
            if (String.IsNullOrEmpty(message) == true)
                message = String.Empty;

            EventType = eventType;
            Message = message;
            Source = source;
            Exception = exception;
        }
    }
}
