﻿using System.IO;

namespace ShutdownTimer.Utils.Helpers
{
    public static class DirectoryHelpers
    {
        public static void CreateDirectoryIfNotExists(string path)
        {
            if(Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
        }

        public static void PurgeDirectory(string path)
        {
            if (Directory.Exists(path) == true)
                Directory.Delete(path, true);
            Directory.CreateDirectory(path);
        }
    }
}
