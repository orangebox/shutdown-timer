﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using ShutdownTimer.Client.App.Views;
using ShutdownTimer.Client.Backend.Data;
using ShutdownTimer.Client.Backend.Design;
using ShutdownTimer.Client.Backend.ViewModels;

namespace ShutdownTimer.Client.App.ViewModels
{
    public class ViewModelLocator
    {
        public PingViewModel Ping { get { return ServiceLocator.Current.GetInstance<PingViewModel>(); } }
        public ParamsViewModel Params { get { return ServiceLocator.Current.GetInstance<ParamsViewModel>(); } }
        public TimerViewModel Timer { get { return ServiceLocator.Current.GetInstance<TimerViewModel>(); } }
        public ExceptionViewModel Exception { get { return ServiceLocator.Current.GetInstance<ExceptionViewModel>(); } }

        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic == true)
            {
                SimpleIoc.Default.Register<IDataService, DesignDataService>();
                SimpleIoc.Default.Register<INavigationService, DesignNavigationService>();
                SimpleIoc.Default.Register<IDialogService, DesignDialogService>();
            }//if
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
                SimpleIoc.Default.Register<INavigationService>(() => CreateNavigationService());
                SimpleIoc.Default.Register<IDialogService, DialogService>();
            }//else

            SimpleIoc.Default.Register<PingViewModel>();
            SimpleIoc.Default.Register<ParamsViewModel>();
            SimpleIoc.Default.Register<TimerViewModel>();
            SimpleIoc.Default.Register<ExceptionViewModel>();
        }//public ViewModelLocator()

        private INavigationService CreateNavigationService()
        {
            var nav = new NavigationService();
            nav.Configure(typeof(PingViewModel).Name, typeof(PingPage));
            nav.Configure(typeof(ParamsViewModel).Name, typeof(ParamsPage));
            nav.Configure(typeof(TimerViewModel).Name, typeof(TimerPage));
            nav.Configure(typeof(ExceptionViewModel).Name, typeof(ExceptionPage));
            return nav;
        }

    }//public class ViewModelLocator
}//namespace ShutdownTimer.Client.App.ViewModels
