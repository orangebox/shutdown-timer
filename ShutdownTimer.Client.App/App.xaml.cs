﻿using ShutdownTimer.Client.App.Views;
using ShutdownTimer.Common.Helpers;
using System;
using System.ServiceModel;
using System.Text;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace ShutdownTimer.Client.App
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        private Frame rootFrame;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.UnhandledException += OnUnhandledException;
        }

        #region Overrides and Events
        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
            var winSize = new Size(752.0, 483.0);
            ApplicationView.PreferredLaunchViewSize = winSize;
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(winSize);
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif
            SetUiTitleBar();
            rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                LoadView(typeof(ParamsPage), e.Arguments);
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }
        
        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        private void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var isShown = false;
            var exceptions = e.Exception.Flatten();
            exceptions.ForEach(ex =>
            {
                Action<string> printLine = line =>
                {
                    System.Diagnostics.Debug.WriteLine(line);
                };
                e.Handled = true;
                printLine($"Type: {ex.GetType().Name}");
                printLine($"Message: {ex.Message}");
                printLine($"Stack Trace:\n{ex.StackTrace}");
                if(isShown == false)
                {
                    LoadView(typeof(ExceptionPage), exceptions);
                    isShown = true;
                }
            });
            //LoadView(typeof(ExceptionPage), exceptions);
            //e.Handled = true;
        }
        #endregion Overrides and Events

        #region Private methods
        private void SetUiTitleBar()
        {
            var getColor = new Func<byte, byte, byte, Windows.UI.Color>((r, g, b) =>
            {
                return new Windows.UI.Color { R = r, G = g, B = b };
            });

            var buttonHoverBrushLight = getColor(99, 99, 99);
            var backgroundBrush = getColor(61, 61, 61);
            var buttonHoverBrushDark = getColor(48, 48, 48);
            var view = ApplicationView.GetForCurrentView();

            view.TitleBar.BackgroundColor = backgroundBrush;
            view.TitleBar.ForegroundColor = Windows.UI.Colors.White;

            view.TitleBar.ButtonBackgroundColor = backgroundBrush;
            view.TitleBar.ButtonForegroundColor = view.TitleBar.ForegroundColor;

            view.TitleBar.ButtonHoverBackgroundColor = buttonHoverBrushLight;
            view.TitleBar.ButtonHoverForegroundColor = view.TitleBar.ForegroundColor;
        }

        private void LoadView(Type viewType, object parameter)
        {
            rootFrame.Navigate(viewType, parameter);
        }
        #endregion Private methods
    }
}
