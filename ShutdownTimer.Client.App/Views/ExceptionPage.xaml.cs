﻿using ShutdownTimer.Client.Backend.Models;
using ShutdownTimer.Client.Backend.ViewModels;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ShutdownTimer.Client.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ExceptionPage : Page
    {
        public ExceptionPage()
        {
            this.InitializeComponent();
            this.Loaded += (s, e) =>
              {
                  System.Diagnostics.Debug.WriteLine($"Loaded {this.GetType().Name}");
              };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine($"Navigated to {this.GetType().Name}");
            var exList = e.Parameter as List<Exception>;
            (DataContext as ExceptionViewModel).Exceptions = ExceptionModel.ConvertFromList(exList);
            base.OnNavigatedTo(e);
        }
    }
}
