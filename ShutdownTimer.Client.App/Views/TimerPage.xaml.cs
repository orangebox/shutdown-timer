﻿using ShutdownTimer.Client.Backend.ViewModels;
using ShutdownTimer.Common.Actions;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ShutdownTimer.Client.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TimerPage : Page
    {
        private int currentAdSizeIndex;
        private readonly List<Size> AD_SIZES = new List<Size>()
        {
            //win10 & win8.1 sizes
            //new Size(250.0, 250.0),
            //new Size(300.0, 250.0),
            //new Size(728.0, 90.0),
            //new Size(160.0, 600.0),
            //new Size(300.0, 600.0),
            //win10 mobile & wp8.x
            //new Size(300.0, 50.0),
            new Size(320.0, 50.0),    //this is the size we're using
            //new Size(480.0, 80.0),
            //new Size(640.0, 100.0),
        };

        public TimerPage()
        {
            this.InitializeComponent();

            TimerAd.Loading += (s, e) =>
            {
#if DEBUG
                TimerAd.ApplicationId = "d25517cb-12d4-4699-8bdc-52040c712cab";
                TimerAd.AdUnitId = "10043121";
                TimerAd.AutoRefreshIntervalInSeconds = 5;
#else
                TimerAd.ApplicationId = "5fceca2c-9d6c-4e08-9f77-72301f93a2f5";
                TimerAd.AdUnitId = "269985";
#endif
                var currentSize = new Size(TimerAd.Width, TimerAd.Height);
                currentAdSizeIndex = AD_SIZES.IndexOf(currentSize);

                TimerAd.BorderBrush = new SolidColorBrush(Colors.Transparent);
                TimerAd.BorderThickness = new Windows.UI.Xaml.Thickness(0.0);
            };

#if DEBUG
            if (AD_SIZES.Count > 1)
            {
                TimerAd.PointerPressed += (s, e) =>
                {
                    currentAdSizeIndex++;
                    if (currentAdSizeIndex > AD_SIZES.Count - 1)
                        currentAdSizeIndex = 0;
                    TimerAd.Width = AD_SIZES[currentAdSizeIndex].Width;
                    TimerAd.Height = AD_SIZES[currentAdSizeIndex].Height;
                    TimerAd.Refresh();
                };
                TimerAd.SizeChanged += (s, e) =>
                {
                    ToolTipService.SetToolTip(TimerAd, $"{TimerAd.ActualWidth} × {TimerAd.ActualHeight}");
                };
            }
#endif
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var spa = e.Parameter as ScheduledPowerAction;
            (DataContext as TimerViewModel).ScheduledAction = spa;
            base.OnNavigatedTo(e);
        }
    }
}
