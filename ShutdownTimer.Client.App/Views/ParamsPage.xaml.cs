﻿using ShutdownTimer.Common.Actions;
using System;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ShutdownTimer.Client.App.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ParamsPage : Page
    {
        public ParamsPage()
        {
            this.InitializeComponent();
            DatePicker.MaxYear = new DateTimeOffset(DateTime.Now.Add(ScheduledPowerAction.UPPER_LIMIT));
            DatePicker.MinYear = new DateTimeOffset(DateTime.Now.Add(ScheduledPowerAction.LOWER_LIMIT));
        }
    }
}
